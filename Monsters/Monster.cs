namespace Monsters
{
    public abstract class Monster
    {
        protected string Name { get; set; }
        protected short HitPoints { get; set; }
        protected byte MoveSpeed { get; set; }

        public Monster(string Name, short HitPoints, byte MoveSpeed)
        {
			this.Name = Name;
			this.HitPoints = HitPoints;
			this.MoveSpeed = MoveSpeed;
		}
    }
}