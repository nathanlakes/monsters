# Monsters

This code demonstrates, mostly, the open-closed principle of SOLID. 
A `Monster` is an abstract class; containing properties that are: 
1. applicable to all Monsters, and 
2. operate functionally identically (i.e. no need to rewrite methods). 

Concrete classes extend the Monster abstract class and at least one of the following interfaces:
1. `IMagic`,
2. `IMelee`,
3. `IRanged`. 

These interfaces serve as a contract to implement methods that monsters of that type have. For example; the IMagic interface may require `CastSpell()`; 

This makes the code open for extension (anyone can make a new monster and can even develope an `IPacifist` interface for Monsters that refuse to fight).
But closed for modification. 

# Structure
```
Monsters
  - IMagic.cs
  - IMelee.cs
  - IRanged.cs
  - Monster.cs
MonstersTests
  - MonsterTests.cs
  - MonstersTests.csproj
Monsters.csproj
Program.cs
```

# Running
```
[Inside Root]
> dotnet add package xunit    
> dotnet build
> cd MonstersTests
> dotnet test
```

The first step `dotnet add package xunit` shouldn't be needed; but dotnet core / code isn't pulling down the package via nuget. 